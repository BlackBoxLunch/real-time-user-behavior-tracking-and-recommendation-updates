import Dependencies._

ThisBuild / scalaVersion     := "2.13.12"
ThisBuild / version          := "0.1.0-SNAPSHOT"
ThisBuild / organization     := "com.example"
ThisBuild / organizationName := "example"

lazy val root = (project in file("."))
  .settings(
    name := "zio_rt_behavior_and_recomendation",
    libraryDependencies += munit % Test
  )


libraryDependencies ++= Seq(
  "dev.zio" %% "zio" % "1.0.12",
  "dev.zio" %% "zio-streams" % "1.0.12"
)

// See https://www.scala-sbt.org/1.x/docs/Using-Sonatype.html for instructions on how to publish to Sonatype.
