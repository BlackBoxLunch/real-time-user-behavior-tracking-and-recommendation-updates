import zio._
import zio.console._
import zio.stream._

object RealTimeUserBehavior extends App{
    case class UserAction(userid: String, action: String, timestamp: Long)
    case class UserFeatures(userid: String, features: Map[String, Int])

    val testData: List[UserAction] = List(
        UserAction("user1", "click", 1620000000000L),
        UserAction("user2", "view", 1620000001000L),
        UserAction("user1","like", 1620000002000L),
        UserAction("user2","share", 1620000003000L),
        UserAction("user4","share", 1620000004000L),
        UserAction("user3","share", 1620000005000L),
        UserAction("user4","share", 1620000007000L)
    )

    //Transform UserAction to UserFeature
    def extractFeatures(action: UserAction): UserFeatures = {
        UserFeatures(action.userid, Map(action.action -> 1))
    }

    def run(args: List[String]): URIO[ZEnv, ExitCode] = {
        val userActionsStream: ZStream[Any, Nothing, UserAction] = ZStream.fromIterable(testData)

        val transformedStream: ZStream[Any, Nothing, UserFeatures] =
        userActionsStream.map(extractFeatures)

        val program = for {
        _ <- putStrLn("Original Test Data:")
        _ <- ZIO.foreach(testData)(action => putStrLn(action.toString))
        
        _ <- putStrLn("\nTransformed Data:")
        _ <- transformedStream.foreach(features => putStrLn(features.toString))
        } yield ExitCode.success

        program.orDie
    }
}